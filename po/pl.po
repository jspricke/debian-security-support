# Translation of debian-security-support debconf templates to Polish.
# Copyright (C) 2014
# This file is distributed under the same license as the debian-security-support package.
#
# Michał Kułach <michal.kulach@gmail.com>, 2014.
# Łukasz Dulny <BartekChom@poczta.onet.pl>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: debian-security-support\n"
"Report-Msgid-Bugs-To: debian-security-support@packages.debian.org\n"
"POT-Creation-Date: 2016-06-07 12:13+0200\n"
"PO-Revision-Date: 2016-05-19 18:33+0100\n"
"Last-Translator: Łukasz Dulny <BartekChom@poczta.onet.pl>\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n"
"%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#: ../check-support-status.in:24
#, sh-format
msgid ""
"Unknown DEBIAN_VERSION $DEBIAN_VERSION. Valid values from "
"$DEB_LOWEST_VER_ID and $DEB_NEXT_VER_ID"
msgstr ""
"Nieznane DEBIAN_VERSION $DEBIAN_VERSION. Prawidłowe wartości z "
"$DEB_LOWEST_VER_ID i $DEB_NEXT_VER_ID"

#: ../check-support-status.in:63
msgid "Failed to parse the command line parameters"
msgstr "Nie udało się przetworzyć parametrów wiersza polecenia"

#: ../check-support-status.in:72
#, sh-format
msgid "$name version $VERSION"
msgstr "$name wersja $VERSION"

#: ../check-support-status.in:101
msgid "E: Internal error"
msgstr "E: Błąd wewnętrzny"

#: ../check-support-status.in:117
msgid "E: Need a --type if --list is given"
msgstr "E: Potrzeba --type jeśli podano --list"

#: ../check-support-status.in:130
#, sh-format
msgid "E: Unknown --type '$TYPE'"
msgstr "E: Nieznane --type '$TYPE'"

#: ../check-support-status.in:282
msgid "Future end of support for one or more packages"
msgstr "Zbliża się koniec wsparcia dla co najmniej jednego pakietu"

#: ../check-support-status.in:285
msgid ""
"Unfortunately, it will be necessary to end security support for some "
"packages before the end of the regular security maintenance life "
"cycle."
msgstr ""
"Niestety, konieczne będzie zakończenie wsparcia bezpieczeństwa dla "
"pewnych pakietów jeszcze przed upływem terminu zakończenia wsparcia "
"dystrybucji."

#: ../check-support-status.in:288 ../check-support-status.in:298
#: ../check-support-status.in:308
msgid ""
"The following packages found on this system are affected by this:"
msgstr ""
"Dotyczy to następujących pakietów zainstalowanych w tym systemie:"

#: ../check-support-status.in:292
msgid "Ended security support for one or more packages"
msgstr "Zakończył się okres wsparcia dla co najmniej jednego pakietu"

#: ../check-support-status.in:295
msgid ""
"Unfortunately, it has been necessary to end security support for some "
"packages before the end of the regular security maintenance life "
"cycle."
msgstr ""
"Niestety, konieczne było zakończenie wsparcia bezpieczeństwa dla "
"pewnych pakietów jeszcze przed upływem terminu zakończenia wsparcia "
"dystrybucji."

#: ../check-support-status.in:302
msgid "Limited security support for one or more packages"
msgstr ""
"Ograniczone wsparcia bezpieczeństwa dla co najmniej jednego pakietu"

#: ../check-support-status.in:305
msgid ""
"Unfortunately, it has been necessary to limit security support for "
"some packages."
msgstr ""
"Niestety, konieczne było ograniczenie wsparcia bezpieczeństwa dla "
"pewnych pakietów."

#: ../check-support-status.in:320
#, sh-format
msgid "* Source:$SRC_NAME, will end on $ALERT_WHEN"
msgstr "* Pakiet źródłowy:$SRC_NAME, koniec $ALERT_WHEN"

#: ../check-support-status.in:323
#, sh-format
msgid ""
"* Source:$SRC_NAME, ended on $ALERT_WHEN at version $ALERT_VERSION"
msgstr ""
"* Pakiet źródłowy:$SRC_NAME, zakończono $ALERT_WHEN na wersji "
"$ALERT_VERSION"

#: ../check-support-status.in:326
#, sh-format
msgid "* Source:$SRC_NAME"
msgstr "* Pakiet źródłowy:$SRC_NAME"

#: ../check-support-status.in:330
#, sh-format
msgid "  Details: $ALERT_WHY"
msgstr "  Szczegóły: $ALERT_WHY"

#: ../check-support-status.in:333
msgid "  Affected binary package:"
msgstr "  Powiązany pakiet binarny:"

#: ../check-support-status.in:335
msgid "  Affected binary packages:"
msgstr "  Powiązane pakiety binarne:"

#: ../check-support-status.in:338
#, sh-format
msgid "  - $BIN_NAME (installed version: $BIN_VERSION)"
msgstr "  - $BIN_NAME (wersja zainstalowana: $BIN_VERSION)"
