# Note to translators:
# Some more messages that require translation are in the po/
# directory. Additionally, there's also a manpage at
# "man/check-support-status.txt", in the docbook format. If you
# provide a translated version, use the file name
# "man/check-support-status.txt.$LANG", and it will be added to the
# package.
#
# Catalan translation of debian-security-support's debconf messages
# Copyright © 2024 Free Software Foundation, Inc.
# This file is distributed under the same license as the debian-security-support package.
# poc senderi <pocsenderi@protonmail.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-security-support\n"
"Report-Msgid-Bugs-To: debian-security-support@packages.debian.org\n"
"POT-Creation-Date: 2016-05-12 09:42+0200\n"
"PO-Revision-Date: 2024-11-03 21:46+0100\n"
"Last-Translator: poc senderi <pocsenderi@protonmail.com>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#. Type: text
#. Description
#: ../debian-security-support.templates:2001
msgid "Ended security support for one or more packages"
msgstr "Ha finalitzat el suport de seguretat per a un o més paquets"

#. Type: text
#. Description
#: ../debian-security-support.templates:2001
msgid ""
"Unfortunately, it has been necessary to end security support for some "
"packages before the end of the regular security maintenance life cycle."
msgstr ""
"Desafortunadament, ha estat necessari finalitzar el suport de seguretat "
"d'alguns paquets abans del final del cicle de vida manteniment de seguretat "
"regular."

#. Type: text
#. Description
#. Type: text
#. Description
#. Type: text
#. Description
#: ../debian-security-support.templates:2001
#: ../debian-security-support.templates:3001
#: ../debian-security-support.templates:4001
msgid "The following packages found on this system are affected by this:"
msgstr ""
"Els següents paquets que es troben en aquest sistema es veuen afectats per "
"això:"

#. Type: text
#. Description
#: ../debian-security-support.templates:3001
msgid "Limited security support for one or more packages"
msgstr "Suport limitat de seguretat per a un o més paquets"

#. Type: text
#. Description
#: ../debian-security-support.templates:3001
msgid ""
"Unfortunately, it has been necessary to limit security support for some "
"packages."
msgstr ""
"Desafortunadament, ha estat necessari limitar el suport de seguretat per a "
"alguns paquets."

#. Type: text
#. Description
#: ../debian-security-support.templates:4001
msgid "Future end of support for one or more packages"
msgstr "Final de suport futur per a un o més paquets"

#. Type: text
#. Description
#: ../debian-security-support.templates:4001
msgid ""
"Unfortunately, it will be necessary to end security support for some "
"packages before the end of the regular security maintenance life cycle."
msgstr ""
"Desafortunadament, serà necessari acabar amb el suport de seguretat "
"d'alguns paquets abans que finalitzi el cicle de vida de manteniment de "
"seguretat regular."
